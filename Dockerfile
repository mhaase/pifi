# create custom JRE - built on example taken from Eclipse Temurin
FROM eclipse-temurin:17 as jre-build
# using output from:
## jdeps --ignore-missing-deps --print-module-deps target/pifi-jar-with-dependencies.jar 
# and add jdk.naming.dns (not detected because of reflection usage)
RUN $JAVA_HOME/bin/jlink \
         --add-modules java.base,java.compiler,java.naming,java.sql,jdk.management,jdk.unsupported,jdk.naming.dns\
         --strip-debug \
         --no-man-pages \
         --no-header-files \
         --compress=2 \
         --output /javaruntime

# create the actual application image
FROM debian:bullseye-slim
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
COPY --from=jre-build /javaruntime $JAVA_HOME

# 
RUN mkdir /opt/app
COPY target/pifi-jar-with-dependencies.jar /opt/app/application.jar
CMD ["java", "-jar", "/opt/app/application.jar"]
