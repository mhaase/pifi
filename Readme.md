# pifi

Trying to build a small online game here.

## todos

- [x] player id
- [x] mouse aiming
- [ ] objects
- [ ] metrics about message throughput
  - [x] use grafana provisioning (datasource and dashboard for websockets)
  - [x] properly use docker (docker-compose with Dockerfile for application)
  - [x] custom metrics for websocket messages
  - [ ] add metrics (JVM & websocket) to dashboard(s)

## getting started

``` bash
# build application
mvn clean package

# start containers with application & monitoring
docker compose up
```

## message types

### client -> server

- command
  - movement (UP, DOWN, LEFT, RIGHT)
  - throw
- event (float values only)
  - aim (angle)

### server -> client

- welcome
- gameState update

