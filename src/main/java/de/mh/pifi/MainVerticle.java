package de.mh.pifi;

import de.mh.pifi.boundary.ApiHandler;
import de.mh.pifi.boundary.CommunicationHub;
import de.mh.pifi.control.PifiHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.micrometer.PrometheusScrapingHandler;

public class MainVerticle extends AbstractVerticle {

    private final PifiHandler pifiHandler;
    private final ApiHandler apiHandler;
    private final CommunicationHub communicationHub;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public MainVerticle() {
        this.pifiHandler = new PifiHandler();
        this.apiHandler = new ApiHandler();
        this.communicationHub = new CommunicationHub(pifiHandler);
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {

        Router router = Router.router(vertx);

        router.route(HttpMethod.GET, "/*").handler(StaticHandler.create());
        router.route(HttpMethod.GET, "/api/hello").handler(apiHandler.hello());

        router.route("/metrics").handler(PrometheusScrapingHandler.create());

        HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);
        healthCheckHandler.register("status", promise -> promise.complete(Status.OK()));
        router.route(HttpMethod.GET, "/health").handler(healthCheckHandler);

        vertx.setPeriodic(10, pifiHandler.handleTick());

        vertx.createHttpServer().requestHandler(router).webSocketHandler(communicationHub::handle).listen(8888,
                http -> {
                    if (http.succeeded()) {
                        startPromise.complete();
                        log.info("HTTP server started on port " + http.result().actualPort());
                    } else {
                        startPromise.fail(http.cause());
                        log.error("HTTP server failed to start", http.cause());
                    }
                });
    }
}
