package de.mh.pifi;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.micrometer.MicrometerMetricsOptions;
import io.vertx.micrometer.VertxPrometheusOptions;
import io.vertx.micrometer.backends.BackendRegistries;

public class VertxStarter {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx(
                new VertxOptions().setMetricsOptions(
                        new MicrometerMetricsOptions()
                                .setPrometheusOptions(new VertxPrometheusOptions().setEnabled(true))
                                .setEnabled(true)
                )
        );

        var registry = BackendRegistries.getDefaultNow();

        Counter.builder("ws.rcv.msg.count").description("Count of messages received via websockets").register(registry);
        Counter.builder("ws.sent.msg.count").description("Count of messages sent via websockets").register(registry);

        new ClassLoaderMetrics().bindTo(registry);
        new ProcessorMetrics().bindTo(registry);
        new JvmMemoryMetrics().bindTo(registry);
        new JvmThreadMetrics().bindTo(registry);
        new JvmGcMetrics().bindTo(registry);

        vertx.deployVerticle(new MainVerticle()).onFailure(ex -> {
            vertx.close();
        });
    }
}
