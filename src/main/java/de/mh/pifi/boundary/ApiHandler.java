package de.mh.pifi.boundary;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class ApiHandler {

    public Handler<RoutingContext> hello() {
        return e -> {
            var nameParam = e.request().getParam("name");
            if (nameParam == null) {
                nameParam = "world";
            }
            e.end(new JsonObject().put("hello", nameParam).encode());
        };
    }

}
