package de.mh.pifi.boundary;

import io.vertx.core.Handler;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.mh.pifi.control.PifiHandler;
import de.mh.pifi.control.bean.ActingEntity;
import io.vertx.micrometer.backends.BackendRegistries;
import java.util.Random;

public class CommunicationHub implements Handler<ServerWebSocket> {

    private final PifiHandler pifiHandler;
    public static final Set<String> VALID_COMMANDS = new HashSet<>(Arrays.asList("U", "D", "L", "R", "T"));
    public static final Set<String> VALID_EVENTS = new HashSet<>(Arrays.asList("A"));
    private final String playerIdCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final int playerIdLength = 6;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public CommunicationHub(PifiHandler pifiHandler) {
        this.pifiHandler = pifiHandler;
    }

    @Override
    public void handle(ServerWebSocket socket) {

        var id = generateId();
        var actingEntity = new ActingEntity(socket, id);
        socket.textMessageHandler(messageHandler(actingEntity));
        socket.closeHandler(closeHandler(actingEntity));
        pifiHandler.addActingEntity(actingEntity);
        actingEntity.sendMessage(new JsonObject().put("t", "w").put("pid", id).encode());
    }

    private Handler<String> messageHandler(ActingEntity actingEntity) {
        return message -> {
            registerReceivedWebsocketMessage();
            var parsedMessage = new JsonObject(message);

            switch (parsedMessage.getString("t", "")) { // type of message
                case "c" -> {
                    // command from client
                    var command = parsedMessage.getString("c");
                    var active = "true".equals(parsedMessage.getString("a"));

                    if (!VALID_COMMANDS.contains(command)) {
                        log.warn("unknown command received: [" + message + "]");
                        return;
                    }

                    actingEntity.addCommand(now(), command, active);
                }
                case "e" -> {
                    // event from client
                    var event = parsedMessage.getString("e");
                    var value = Double.valueOf(parsedMessage.getString("v"));

                    if (!VALID_EVENTS.contains(event)) {
                        log.warn("unknown event received: [" + message + "]");
                        return;
                    }

                    actingEntity.addEvent(now(), event, value);
                }
                default -> {
                    log.warn("unknown message received: [" + parsedMessage + "]");
                }
            }
        };
    }

    private Handler<Void> closeHandler(ActingEntity actingEntity) {
        return _void -> {
            pifiHandler.removeActingEntity(actingEntity);
        };
    }

    private long now() {
        return System.currentTimeMillis();
    }

    private String generateId() {
        Random random = new Random();

        StringBuilder id = new StringBuilder(playerIdLength);
        for (int i = 0; i < playerIdLength; i += 1) {
            id.append(playerIdCharacters.charAt(random.nextInt(playerIdCharacters.length())));
        }
        return id.toString();
    }

    private void registerReceivedWebsocketMessage() {
        //TODO: looks ugly, is probably not idiomatic
        var metricsRegistry = BackendRegistries.getDefaultNow();
        if (metricsRegistry == null) {
            return;
        }
        var counter = metricsRegistry
                .find("ws.rcv.msg.count")
                .counter();
        if (counter == null) {
            return;
        }
        counter.increment();
    }
}
