package de.mh.pifi.control;

import java.util.LinkedList;

import de.mh.pifi.control.bean.ActingEntity;
import de.mh.pifi.control.bean.GameState;
import io.vertx.core.Handler;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;

//TODO: this is in fact a lobby, or a game instance
public class PifiHandler {

    private final LinkedList<ActingEntity> queuedActingEntities = new LinkedList<>();
    private final LinkedList<ActingEntity> removedActingEntities = new LinkedList<>();
    private GameState gameState;
    private boolean ticking = false;
    private long lastTick = now();

    private final double tmp_movementSpeed = 100;  //TODO: it's basically pixels per second now, rework to decoupled coordinates

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public PifiHandler() {
        gameState = new GameState(now(), new LinkedList<>(), new LinkedList<>());
    }

    public Handler<Long> handleTick() {
        return timerId -> {
            if (ticking) {
                log.warn("trying to tick, but am still ticking");
                return;
            }
            ticking = true;
            var now = now();
            double elapsed = (double) (now - lastTick) / 1000.;
            lastTick = now;

            var newGameState = new GameState(now, new LinkedList<>(), new LinkedList<>());

            // add waiting new players
            ActingEntity queuedActingEntity;
            while ((queuedActingEntity = queuedActingEntities.poll()) != null) {
                newGameState.players().add(new GameState.Player(queuedActingEntity, 0, 0, 0, new HashSet<>(), true));
            }

            // remove player that have left
            var leavers = new LinkedList<ActingEntity>();
            ActingEntity removedActingEntity;
            while ((removedActingEntity = removedActingEntities.poll()) != null) {
                leavers.add(removedActingEntity);
            }

            // react to commands from clients
            gameState.players().forEach(p -> {

                if (leavers.contains(p.actingEntity())) {
                    return;
                }

                // manually creating a mutable tmp structure here... feels inelegant
                var x = p.x();
                var y = p.y();
                var aimAngle = p.aimAngle();
                var activeCommands = new HashSet<>(p.activeCommands());

                ActingEntity.QueuedCommand queuedCommand;
                var changed = false;
                // drain queued commands for actor
                while ((queuedCommand = p.actingEntity().queuedCommands().poll()) != null) {
                    changed = true;
                    if (queuedCommand.active()) {
                        activeCommands.add(queuedCommand.command());
                    } else {
                        activeCommands.remove(queuedCommand.command());
                    }
                }

                ActingEntity.QueuedEvent queuedEvent;
                // drain queued events for actor
                while ((queuedEvent = p.actingEntity().queuedEvents().poll()) != null) {
                    changed = true;
                    switch (queuedEvent.event()) {
                        case "A" -> {
                            aimAngle = queuedEvent.value();
                        }
                    }
                }

                if (activeCommands.contains("U")) {
                    y -= tmp_movementSpeed * elapsed;
                    changed = true;
                }
                if (activeCommands.contains("D")) {
                    y += tmp_movementSpeed * elapsed;
                    changed = true;
                }
                if (activeCommands.contains("L")) {
                    x -= tmp_movementSpeed * elapsed;
                    changed = true;
                }
                if (activeCommands.contains("R")) {
                    x += tmp_movementSpeed * elapsed;
                    changed = true;
                }
                if (activeCommands.contains("T") && !p.activeCommands().contains("T")) {
                    // only execute if command is "new", so a one-time action
                    double speed = 100;
                    double dx = Math.cos(aimAngle) * speed;
                    double dy = Math.sin(aimAngle) * speed;

                    newGameState.objects().add(new GameState.Object(x, y, dx, dy, 10, true, p.actingEntity(), Instant.now(), Duration.ofSeconds(2))); //TODO: reuse "now" instance from method scope
                }

                newGameState.players().add(new GameState.Player(p.actingEntity(), x, y, aimAngle, activeCommands, changed));
            });

            gameState.objects().forEach(o -> {
                //TODO: reuse "now" instance from method scope
                if (o.createdAt().plus(o.lifetime()).isBefore(Instant.now())) {
                    // end of this object, could introduce animation or effect or something
                    return;
                }
                double x = o.x();
                double y = o.y();

                x += o.dx() * elapsed;
                y += o.dy() * elapsed;

                newGameState.objects().add(new GameState.Object(x, y, o.dx(), o.dy(), o.radius(), true, o.creator(), o.createdAt(), o.lifetime()));
            });

            //
            var updateNeeded = newGameState.players().stream().filter(GameState.Player::changed).findAny().isPresent()
                    || newGameState.objects().stream().filter(GameState.Object::changed).findAny().isPresent();
            if (updateNeeded) {
                updatePlayersGameState(newGameState);
            }

            gameState = newGameState;
            ticking = false;
        };
    }

    private void updatePlayersGameState(GameState newGameState) {
        var gameStateMessageBuilder = new JsonObject()
                .put("t", "g")
                .put(
                        "p",
                        newGameState.players()
                                .stream()
                                .map(
                                        p -> new JsonObject()
                                                .put("id", p.actingEntity().id())
                                                .put("x", round(p.x()))
                                                .put("y", round(p.y()))
                                                .put("a", round(p.aimAngle()))
                                )
                                .toList()
                )
                .put(
                        "o",
                        newGameState.objects()
                                .stream()
                                .map(
                                        o -> new JsonObject()
                                                .put("x", o.x())
                                                .put("y", o.y())
                                                .put("dx", o.dx())
                                                .put("dy", o.dy())
                                                .put("r", o.radius())
                                )
                                .toList()
                );

        var gameStateMessage = gameStateMessageBuilder.encode();
        newGameState.players().forEach(p -> {
            p.actingEntity().sendMessage(gameStateMessage);
        });
    }

    private double round(double d) {
        return (double) ((int) (d * 100.)) / 100.;
    }

    public void addActingEntity(ActingEntity actingEntity) {
        queuedActingEntities.add(actingEntity);
    }

    private long now() {
        //TODO: use instant, to get higher resolution and better precision
        return System.currentTimeMillis();
    }

    public void removeActingEntity(ActingEntity actingEntity) {
        removedActingEntities.add(actingEntity);
    }
}
