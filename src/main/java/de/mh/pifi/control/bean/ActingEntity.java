package de.mh.pifi.control.bean;

import io.vertx.core.http.ServerWebSocket;
import io.vertx.micrometer.backends.BackendRegistries;
import java.util.LinkedList;

public record ActingEntity(ServerWebSocket websocket, String id, LinkedList<QueuedCommand> queuedCommands, LinkedList<QueuedEvent> queuedEvents) {

    public ActingEntity(ServerWebSocket websocket, String id) {
        this(
                websocket,
                id,
                new LinkedList<>(),
                new LinkedList<>()
        );
    }

    public void sendMessage(String message) {
        websocket.writeTextMessage(message).onComplete(result -> registerSentWebsocketMessage());

    }

    public void addCommand(long timestamp, String command, boolean active) {
        queuedCommands.add(new QueuedCommand(timestamp, command, active));
    }

    public void addEvent(long timestamp, String event, double value) {
        queuedEvents.add(new QueuedEvent(timestamp, event, value));
    }

    public record QueuedCommand(long timestamp, String command, boolean active) {

    }

    public record QueuedEvent(long timestamp, String event, double value) {

    }

    private void registerSentWebsocketMessage() {
        //TODO: looks ugly, is probably not idiomatic
        var metricsRegistry = BackendRegistries.getDefaultNow();
        if (metricsRegistry == null) {
            return;
        }
        var counter = metricsRegistry
                .find("ws.sent.msg.count")
                .counter();
        if (counter == null) {
            return;
        }
        counter.increment();
    }
}
