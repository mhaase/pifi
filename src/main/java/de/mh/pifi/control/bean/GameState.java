package de.mh.pifi.control.bean;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Set;

public record GameState(long timestamp, List<Player> players, List<Object> objects) {

    public record Player(ActingEntity actingEntity, double x, double y, double aimAngle, Set<String> activeCommands, boolean changed) {

    }

    public record Object(double x, double y, double dx, double dy, double radius, boolean changed, ActingEntity creator, Instant createdAt, Duration lifetime) {

    }
}
